//
//  KKEnemy.h
//  hogefuga
//
//  Created by Kenji Kinukawa on 2013/02/24.
//  Copyright (c) 2013年 Kenji Kinukawa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KKEnemy : NSObject
@property (nonatomic,assign) NSInteger hitPoint;
@property (nonatomic,assign) NSInteger attackPoint;
- (NSInteger)attack;
- (void)receive:(NSInteger)damagePoint;
- (BOOL)isDead;
@end
