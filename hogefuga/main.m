//
//  main.m
//  hogefuga
//
//  Created by Kenji Kinukawa on 2013/02/24.
//  Copyright (c) 2013年 Kenji Kinukawa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KKAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KKAppDelegate class]));
    }
}
