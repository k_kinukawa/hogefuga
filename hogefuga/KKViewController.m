//
//  KKViewController.m
//  hogefuga
//
//  Created by Kenji Kinukawa on 2013/02/24.
//  Copyright (c) 2013年 Kenji Kinukawa. All rights reserved.
//

#import "KKViewController.h"
#import "KKEnemy.h"

@interface KKViewController ()
@property (nonatomic,strong) KKEnemy *dog;
@property (nonatomic,strong) KKEnemy *cat;

@property (nonatomic,weak) IBOutlet UILabel *catHitPoint;
@property (nonatomic,weak) IBOutlet UILabel *catAttackPoint;
@property (nonatomic,weak) IBOutlet UILabel *dogHitPoint;
@property (nonatomic,weak) IBOutlet UILabel *dogAttackPoint;

- (IBAction)dogAttack:(id)sender;
- (IBAction)catAttack:(id)sender;
@end

@implementation KKViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.dog = [[KKEnemy alloc]init];
    self.cat = [[KKEnemy alloc]init];

    _dog.attackPoint = 10;
    _dog.hitPoint = 200;

    _cat.attackPoint = 20;
    _cat.hitPoint = 100;

    [self refreshPoints];
}

//これは、数字をもらったら文字列にするだけで
- (NSString *)pointStringWithInteger:(NSInteger)point
{
    NSString *string = [NSString stringWithFormat:@"%d",point];
    return string;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dogAttack:(id)sender
{
    [self attackToTarget:_cat attacker:_dog];
}

- (IBAction)catAttack:(id)sender
{
    [self attackToTarget:_dog attacker:_cat];
}

- (void)attackToTarget:(KKEnemy *)target attacker:(KKEnemy *)attacker
{
    [target receive:attacker.attackPoint];
    [self refreshPoints];
}

- (void)refreshPoints
{
    _catAttackPoint.text = [self pointStringWithInteger:_cat.attackPoint];
    _catHitPoint.text = [self pointStringWithInteger:_cat.hitPoint];
    _dogAttackPoint.text = [self pointStringWithInteger:_dog.attackPoint];
    _dogHitPoint.text = [self pointStringWithInteger:_dog.hitPoint];
}
@end
