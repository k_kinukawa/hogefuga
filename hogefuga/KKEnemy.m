//
//  KKEnemy.m
//  hogefuga
//
//  Created by Kenji Kinukawa on 2013/02/24.
//  Copyright (c) 2013年 Kenji Kinukawa. All rights reserved.
//

#import "KKEnemy.h"

@implementation KKEnemy

- (NSInteger)attack
{
    NSInteger damerge;
    damerge = _attackPoint;
    return damerge;
}

- (void)receive:(NSInteger)damagePoint
{
    _hitPoint = _hitPoint - damagePoint;
}

- (BOOL)isDead
{
    if (_hitPoint < 0) {
        return YES;
    }
    return NO;
}
@end
