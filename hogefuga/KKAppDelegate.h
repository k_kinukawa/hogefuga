//
//  KKAppDelegate.h
//  hogefuga
//
//  Created by Kenji Kinukawa on 2013/02/24.
//  Copyright (c) 2013年 Kenji Kinukawa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
